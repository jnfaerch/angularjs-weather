//Module
var weatherApp = angular.module('weatherApp', ['ngRoute', 'ngResource']);

//Routes
weatherApp.config(function ($routeProvider) {

  $routeProvider

  .when('/', {
    templateUrl: 'pages/home.html',
    controller: 'homeController'
  })

  .when('/forecast', {
    templateUrl: 'pages/forecast.html',
    controller: 'forecastController'
  })

  .when('/forecast/:days', {
    templateUrl: 'pages/forecast.html',
    controller: 'forecastController'
  })

});

//Services
weatherApp.service('cityService', function() {
  this.city = 'Malmö, SE';
});

//Controllers
weatherApp.controller('homeController', ['$scope', 'cityService', function($scope, cityService) {

  $scope.city = cityService.city;

  $scope.$watch('city', function() {
    cityService.city = $scope.city;
  });

}]);

weatherApp.controller('forecastController', ['$scope', '$resource', '$filter', '$routeParams', 'cityService', function($scope, $resource, $filter, $routeParams, cityService) {

  $scope.city = cityService.city;

  $scope.days = $routeParams.days || '5';

  $scope.weatherAPI = $resource("https://api.openweathermap.org/data/2.5/forecast?APPID=5de5df468a5b34b8eb6541ec41382a55", { callback: "JSON_CALLBACK" }, { get: { method: "JSONP" }});

  $scope.weatherResult = $scope.weatherAPI.get({ q: $scope.city, cnt: $scope.days });

  $scope.convertToCelsius = function(degC) {
    return $filter("number")((degC - 273.15), 1);
  }

  console.log($scope.weatherResult);

}]);
